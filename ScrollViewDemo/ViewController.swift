//
//  ViewController.swift
//  ScrollViewDemo
//
//  Created by Kapil Dhawan on 21/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfFatherName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfYear: UITextField!
    @IBOutlet weak var tfBranch: UITextField!
    @IBOutlet weak var tfGrade: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
   
    
    @IBAction func btnSignup(_ sender: UIButton) {
        
        if !( tfName.text!.isEmpty || tfFatherName.text!.isEmpty || tfEmail.text!.isEmpty ||  tfYear.text!.isEmpty || tfBranch.text!.isEmpty || tfGrade.text!.isEmpty || tfPassword.text!.isEmpty || tfConfirmPassword.text!.isEmpty ){
            
            let testEmail = tfEmail.text!
            let year: Int? = Int(tfYear.text!)
            if !isValidEmail(testEmail){
                
                print("Enter a valid email")
               
            }else if !(year! >= 2000 && year! <= 2050){
                print("Enter a valid year")
                
            }
            else if !(tfPassword.text! == tfConfirmPassword.text!){
                print("Password does not match")
            }
        
        else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "ScrollViewVC") as! ScrollViewVC
        data.getName = tfName.text!
        data.getEmail = tfEmail.text!
        data.getYear = tfYear.text!
        data.getBranch = tfBranch.text!
        data.getGrade = tfGrade.text!
        self.navigationController?.pushViewController(data, animated: true)
            clearData()
            }
          }else{
            print("Fill all the textfields")
            }
        
    }
    func clearData(){
 
        tfName.text! = ""
        tfFatherName.text! = ""
        tfEmail.text! = ""
        tfYear.text! = ""
        tfBranch.text! = ""
        tfGrade.text! = ""
        tfPassword.text! = ""
        tfConfirmPassword.text! = ""
    }
    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}

