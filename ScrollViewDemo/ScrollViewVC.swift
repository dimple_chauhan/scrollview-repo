//
//  ScrollViewVC.swift
//  ScrollViewDemo
//
//  Created by Kapil Dhawan on 21/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class ScrollViewVC: UIViewController {
    var getName = String()
    var getEmail = String()
    var getYear = String()
    var getBranch = String()
    var getGrade = String()


    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblBranch: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       lblName.text = getName
       lblEmail.text = getEmail
       lblYear.text = getYear
       lblBranch.text = getBranch
       lblGrade.text = getGrade

    }
    

}
